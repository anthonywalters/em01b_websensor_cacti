# get_em01_data.pl perl script

This script was written for use with cacti and reads temperature, humidity and illumination 
data from an em01b websensor and outputs the data in a format that cacti can process.

## Script dependencies

This script depends on the LWP::Simple perl module
To install, run the following apt command
```
apt-get install libwww-perl
```

## Usage

Run the script and supply the ip address of the hvac unit at the command line
e.g.
```
get_em01_data.pl 10.0.0.10
```

## License
This software is licensed under the **GNU General Public License v3.0**
