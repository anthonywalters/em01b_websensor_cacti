#!/usr/bin/perl

#     Copyright (C) 2017 Anthony Walters <anthony.walters@gmx.com>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This script gets temperature, humidity and luminosity reading from an em01b websensor
# and outputs in a format that cacti can process

# Written by Anthony Walters August 2017
# anthony.walters@gmx.com

#
# This script uses the LWP::Simple perl modules for connecting to web servers
# install libwww-perl to add LWP::Simple
#   apt-get install libwww-perl

#Usage:
# ./get_em01_data.pl <ip>
# e.g.
# ./get_em01_data.pl 10.0.0.10
#

use strict;
use LWP::Simple;
use HTML::FormatText 2;

my $url;
my $plain_content;
my $html_content;

$url = "http://" . $ARGV[0] . "/index.html?em";

$html_content = get $url;

# if we haven't got any data, just end the script
die "Couldn't get $url" unless defined $html_content;
 
# strip html tags
$plain_content = HTML::FormatText->format_string($html_content);

#Using parentheses in the regex below, the temperature, humidity and illumination 
#are put into the capture variables $1, $2, and $3 respectively
$plain_content =~ /^.*TC:\s*([0-9\.]*)HU:\s*([0-9\.]*)%IL\s*([0-9\.]*).*$/; 

#print out the variables captured from the regex above in a format that cacti can read
print "Temp:";
printf('%.2f', $1);
print " Humid:";
printf('%.2f', $2);
print " Illum:";
printf('%.2f', $3);
print "\n";

exit;

